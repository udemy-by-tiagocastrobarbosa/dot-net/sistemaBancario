﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBancario.Model
{
    class Banco : Operacao
    {
        public long codigo { get; set; }
        public List<Agencia> agencias { get; set; }
        public double montante { get; private set; }

        public Banco(long codigo)
        {
            this.codigo = codigo;
            montante = 0.0;
            Program.bancos.Add(this); // apenas porque não tô usando banco de dados :/
        }

        public override bool Depositar(double valor)
        {
            montante += valor;
            return true;
        }

        public override bool Sacar(double valor)
        {
            montante -= valor;
            return true;
        }

        public double GetMontante()
        {
            return montante;
        }
    }
}
